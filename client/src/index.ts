import net from 'net'

const port = 8080;
const host = "localhost"

const client = new net.Socket();

client.connect({ port, host }, () => {
    console.log("Connected with server")

    client.write("Hello")
    client.end()
})

client.on("end",  () => {
    console.log("End to connection")
})