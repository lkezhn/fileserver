import * as net from 'net'

const port = 8080;

const server : net.Server = new net.Server();

server.listen(port, () => {
    console.log("Server's up on localhost:" + port)
})

server.on('connection', (socket : net.Socket) => {
    socket.write("Hello, client");

    socket.on('data', (chunk: Buffer) => {
        console.log("Info: "+ chunk.toString())
    })
} )